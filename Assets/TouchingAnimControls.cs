﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchingAnimControls : MonoBehaviour
{   string AreaName;
    int Happy = 5;
    Animator animator;


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void CheckHit(Vector2 screenPosition)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        RaycastHit Hit;
        if (Physics.Raycast(ray, out Hit))
        {
            AreaName = Hit.transform.name;
            switch (AreaName)
            {
                case "Head_Cube":
                    Happy++;
                    animator.SetFloat("FeelingLevel", Happy);
                    break;
                case "Bottom_Cube":
                    Happy--;
                    animator.SetFloat("FeelingLevel", Happy);
                    break;
                default:
                    break;


            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            CheckHit(Input.GetTouch(0).position);
        }
        else if(Input.GetMouseButtonDown(0))
        {
            CheckHit(Input.mousePosition);
        }
    }
}
